masajeo de la data de Rosario
Intento sacar de en toto5 ~ psql -h 172.26.67.64 toto5 -U halpe con la siguiente consulta

select 
-- campos, columnas stuff (fields)
radio.id id, radio.codigo radio, localidad.codigo, localidad.nombre localidad, geom

from 
-- corelation
radio 
join tipo_de_radio on tipo_de_radio.id = tipo_de_radio_id
join radio_localidad on radio_id = radio.id
join localidad on localidad.id = localidad_id
join aglomerados on aglomerados.id = aglomerado_id
join geometria on geometria.id = radio.geometria_id

where 
-- condition
aglomerados.codigo = '0003' -- Gran Rosario
and 
tipo_de_radio.nombre = 'U' -- solo radios urbanos
;
Pero decido pedirle los shapes a Emanuel

Edited 2 days ago by Alperin Hernán
 
Alperin Hernán 💬 @halperin changed the description 2 times within 3 minutes 1 month ago
Alperin Hernán 💬 @halperin changed the description 1 month ago
Alperin Hernán 💬 @halperin changed the description 2 times within 2 minutes 1 month ago
Alperin Hernán
Alperin Hernán 💬 @halperin · 3 weeks ago
Maintainer
Se decidió usar la data provista por Emanuel

C:\Users\halperin\Downloads\data_radios\data_radios\shapes santa fe INDEC

Los shapes se cargaron a la base PSU de 172.26.68.174 usando QGIS.

y los datos_radio.csv

Columna	Definición
id_merge	Identificador de radio censal. Es concatenación de códigos de PROVINCIA, DEPARTEMENTO, FRACCION y RADIO
ocupado	Total de personas ocupadas en el radio
desocupado	Total de personas desocupadas en el radio
activo	Total de personas activas en el radio
tamanio_hogar	Total de hogares en viviendas ocupadas en el radio
hombres	Total de hombres en el radio
mujeres	Total de mujeres en el radio
hombres14a29	Total de hombres de 14 a 29 años de edad en el radio
hombres30a64	Total de hombres de 30 a 64 años de edad en el radio
hombres65a120	Total de hombres de 65 a 120 años de edad en el radio
mujeres14a29	Total de mujeres de 14 a 29 años de edad en el radio
mujeres30a64	Total de mujeres de 30 a 64 años de edad en el radio
mujeres65a120	Total de mujeres de 65 a 120 años de edad en el radio
nivel_ed_ini	Total de personas con educacion inicial en el radio (definida como cursa o curso educacion inicial, primaria, EGB o educacion especial)
nivel_ed_sec	Total de personas con educacion inicial en el radio (definida como cursa o curso educacion secundaria o polimodal)
nivel_ed_sup	Total de personas con educacion inicial en el radio (definida como cursa o curso educacion superior, universitaria o post universitaria)
Edited by Alperin Hernán 3 weeks ago
Alperin Hernán
Alperin Hernán 💬 @halperin · 3 weeks ago
Maintainer
Crear la tabla para importar la data en csv

CREATE TABLE datos_radio (
  i integer,
  id_merge char(9),
  ocupado integer,
  desocupado integer,
  activo  integer,
  tamanio_hogar integer,
  hombres integer,
  mujeres integer,
  hombres14a29 integer,
  hombres30a64 integer,
  hombres65a120 integer,
  mujeres14a29 integer,
  mujeres30a64 integer,
  mujeres65a120 integer,
  nivel_ed_ini integer,
  nivel_ed_sec integer,
  nivel_ed_sup integer
  )
;
Edited by Alperin Hernán 3 weeks ago
Alperin Hernán
Alperin Hernán 💬 @halperin · 3 weeks ago
Maintainer
los datos se cargaron desde data.sql un archivo .csv a .sql masajeado con vi en comandos

INSERT INTO datos_radio VALUES
(...),
...
(...)
;
Alperin Hernán
Alperin Hernán 💬 @halperin · 3 weeks ago
Maintainer
➜  PSU psql -h 172.26.68.174 PSU -U halpe
psql (12.10, servidor 11.5)
Digite «help» para obtener ayuda.

PSU=# \dt
           Listado de relaciones
 Esquema |     Nombre      | Tipo  | Dueño
---------+-----------------+-------+-------
 public  | datos_radio     | tabla | halpe
 public  | radios_santa_fe | tabla | halpe
 public  | spatial_ref_sys | tabla | halpe
(3 filas)

PSU=# \d radios_santa_fe
                                              Tabla «public.radios_santa_fe»
   Columna    |             Tipo             | Ordenamiento | Nulable  |                   Por omisión
--------------+------------------------------+--------------+----------+--------------------------------------------------
 ogc_fid      | integer                      |              | not null | nextval('radios_santa_fe_ogc_fid_seq'::regclass)
 wkb_geometry | geometry(MultiPolygon,22183) |              |          |
 toponimo_i   | numeric(10,0)                |              |          |
 link         | character varying(254)       |              |          |
 varon        | numeric(24,15)               |              |          |
 mujer        | numeric(24,15)               |              |          |
 totalpobl    | numeric(24,15)               |              |          |
 hogares      | numeric(24,15)               |              |          |
 viviendasp   | numeric(24,15)               |              |          |
 viv_part_h   | numeric(24,15)               |              |          |
Índices:
    "radios_santa_fe_pk" PRIMARY KEY, btree (ogc_fid)
    "radios_santa_fe_wkb_geometry_geom_idx" gist (wkb_geometry)

PSU=# \d datos_radio
                     Tabla «public.datos_radio»
    Columna    |     Tipo     | Ordenamiento | Nulable | Por omisión
---------------+--------------+--------------+---------+-------------
 i             | integer      |              |         |
 id_merge      | character(9) |              |         |
 ocupado       | integer      |              |         |
 desocupado    | integer      |              |         |
 activo        | integer      |              |         |
 tamanio_hogar | integer      |              |         |
 hombres       | integer      |              |         |
 mujeres       | integer      |              |         |
 hombres14a29  | integer      |              |         |
 hombres30a64  | integer      |              |         |
 hombres65a120 | integer      |              |         |
 mujeres14a29  | integer      |              |         |
 mujeres30a64  | integer      |              |         |
 mujeres65a120 | integer      |              |         |
 nivel_ed_ini  | integer      |              |         |
 nivel_ed_sec  | integer      |              |         |
 nivel_ed_sup  | integer      |              |         |

PSU=# select count(*) from radios_santa_fe;
 count
-------
  3779
(1 fila)

PSU=# select count(*) from radios_santa_fe join datos_radio on id_merge = link;
 count
-------
  1069
(1 fila)

Edited by Alperin Hernán 3 weeks ago
Alperin Hernán 💬 @halperin changed title from obtención de radios 2010 con geometría de Rosario to masajeo de la data de Rosario 3 weeks ago
Alperin Hernán 💬 @halperin changed the description 3 weeks ago
Alperin Hernán
Alperin Hernán 💬 @halperin · 2 weeks ago
Maintainer
PSU=# alter table radios_rosario add column psu integer;
agrego columna para asigna psu a cada radio

PSU=# grant select on radios_rosario to ggarcia, psu;
GRANT
PSU=# grant select on adyacencias to ggarcia, psu;
GRANT
PSU=#
doy permisos de consulta

Edited by Alperin Hernán 2 weeks ago
Alperin Hernán 💬 @halperin changed the description 2 days ago
Alperin Hernán
Alperin Hernán 💬 @halperin · 2 days ago
Maintainer
Agrego cantidad de viviendas particulares

PSU=# alter table radios_rosario add column vivs integer;
ALTER TABLE
PSU=# update radios_rosario set vivs = viv_part_h from radios_santa_fe where link = codigo;
UPDATE 1069

PSU=# select min(vivs), percentile_disc(0.1) within group (order by vivs)as "10%", percentile_disc(0.5) within group (order by vivs)as "median", round(avg(vivs)) as "avg", percentile_disc(0.9) within group (order by vivs)as "90%", percentile_disc(0.95) within group (order by vivs)as "95%", percentile_disc(0.99) within group (order by vivs)as "99%", max(vivs) from radios_rosario;
 min | 10% | median | avg | 90% | 95% | 99% | max
-----+-----+--------+-----+-----+-----+-----+------
   4 | 165 |    266 | 271 | 376 | 417 | 546 | 1255
(1 row)
Se puede empezar con los radios grandes separados en respectivos cluster unitarios ...?

PSU=#  select min(max_x - min_x), percentile_cont(0.1) within group (order by (max_x - min_x)) as "10%", round(avg(max_x - min_x)) as "avg", percentile_cont(0.9) within group (order by (max_x - min_x)) as "90%", max(max_x - min_x) from radios_rosario;
     min      |     10%      | avg |     90%      |      max
--------------+--------------+-----+--------------+---------------
 117.66603103 | 168.32083842 | 515 | 916.87009934 | 4948.21287375
(1 fila)

select min(max_y - min_y), percentile_cont(0.1) within group (order by (max_y - min_y)) as "10%", round(avg(max_y - min_y)) as "avg", percentile_cont(0.9) within group (order by (max_y - min_y)) as "90%", max(max_y - min_y) from radios_rosario;

     min     |     10%      | avg |     90%      |      max
-------------+--------------+-----+--------------+---------------
 88.23566221 | 164.00587921 | 379 | 596.09421458 | 4007.14816627
(1 fila)
Edited by Alperin Hernán 10 hours ago
Alperin Hernán
Alperin Hernán 💬 @halperin · 12 hours ago
Maintainer
eliminamos PPDDD porque son todos de Rosario 81084

begin;
update radios_rosario set codigo = substr(codigo,6,4);
update adyacencias set codigo_i = substr(codigo_i,6,4);
update adyacencias set codigo_j = substr(codigo_j,6,4);
commit;
esos campos son char(9) ...
cambio campos ffrr de tipo char(4)

alter table radios_rosario add column ffrr char(4);
alter table adyacencias add column ffrr_i char(4);
alter table adyacencias add column ffrr_j char(4);
update radios_rosario set ffrr = codigo;
update adyacencias set ffrr_i = codigo_i;
update adyacencias set ffrr_j = codigo_j;
ver casos de vecindad problemática

with ambos_lados as (
  select ffrr_i, ffrr_j
  from adyacencias
  union
  select ffrr_j, ffrr_i
  from adyacencias
)
select codigo, vivs, desocupado, 
    cardinality(array_agg(distinct ffrr_j)), 
    array_agg(distinct ffrr_j order by ffrr_j)
from radios_rosario
join ambos_lados
on codigo = ffrr_i
group by codigo, vivs, desocupado
order by 4, vivs desc
;
Vemos los indicadores

select 
  min(100*desocupado/vivs),
  percentile_disc(0.5) within group (order by 100*desocupado/vivs) as median,
  100*sum(desocupado)/sum(vivs) as avg,
  max(100*desocupado/vivs)
from radios_rosario
;
 min | median | avg | max
-----+--------+-----+-----
   0 |      9 |  11 |  57
(1 row)
